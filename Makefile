# Image URL to use all building/pushing image targets
# WARNING: This version number is also used by CI to tag generated docker images

# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Build manager binary
manager: fmt vet
	go build -o bin/manager cmd/controller-manager/main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
run: manager
	bin/manager --resync=5m --revisionHistoryLimit=3

############################
# DEVELOPMENT ONLY TARGETS #
############################

VERSION ?= 0.0.999
IMG ?= docker.io/kumoridev/solution-controller:${VERSION}

# Build the docker image
#
# IMPORTANT: check Dockerfile first (some actions required before)
#
docker-build:
	docker build . -t ${IMG}

# Push the docker image
docker-push:
	docker login
	docker push ${IMG}
	docker logout

# Push the docker image to a development cluster
docker-push-dev:
	kind load docker-image $IMG $IMG --name devtesting
