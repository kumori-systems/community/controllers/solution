/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package solutioncontroller

import (
	"fmt"

	history "solution-controller/pkg/controllers/kukusolution/history"
	common "solution-controller/pkg/utils/common"
	kerrors "solution-controller/pkg/utils/errors"
	kukulinktools "solution-controller/pkg/utils/kukulink"
	resourcetools "solution-controller/pkg/utils/resources"
	v3deptools "solution-controller/pkg/utils/v3dep"

	log "github.com/sirupsen/logrus"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	errors "k8s.io/apimachinery/pkg/api/errors"
	kubeinformers "k8s.io/client-go/informers/apps/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	kubelisters "k8s.io/client-go/listers/apps/v1"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"
)

// KukuSolutionController is the struct representing the controller
type KukuSolutionController struct {
	base.BaseController
	RevisionHistoryLimit int // JJJ TODO configmap!
	kubeClientset        kubeclientset.Interface
	kukuClientset        kukuclientset.Interface
	kukuSolutionInformer kumoriinformers.KukuSolutionInformer
	kukuSolutionLister   kumorilisters.KukuSolutionLister
	v3DepInformer        kumoriinformers.V3DeploymentInformer
	v3DepLister          kumorilisters.V3DeploymentLister
	v3DepTools           v3deptools.V3DepTools
	kukuLinkInformer     kumoriinformers.KukuLinkInformer
	kukuLinkLister       kumorilisters.KukuLinkLister
	kukuLinkTools        kukulinktools.KukuLinkTools
	revisionInformer     kubeinformers.ControllerRevisionInformer
	revisionsLister      kubelisters.ControllerRevisionLister
	controllerHistory    history.Interface
	resourceTools        resourcetools.ResourceTools
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kukuClientset kukuclientset.Interface,
	kukuSolutionInformer kumoriinformers.KukuSolutionInformer, // Primary
	v3DepInformer kumoriinformers.V3DeploymentInformer, // Secondary
	kukuLinkInformer kumoriinformers.KukuLinkInformer, // Secondary
	revisionInformer kubeinformers.ControllerRevisionInformer, // Special secondary
	kukuSecretInformer kumoriinformers.KukuSecretInformer, // Terciary
	kukuVolumeInformer kumoriinformers.KukuVolumeInformer, // Terciary
	kukuDomainInformer kumoriinformers.KukuDomainInformer, // Terciary
	kukuCertInformer kumoriinformers.KukuCertInformer, // Terciary
	kukuCaInformer kumoriinformers.KukuCaInformer, // Terciary
	kukuPortInformer kumoriinformers.KukuPortInformer, // Terciary
	revisionHistoryLimit int,
) (c *KukuSolutionController) {
	controllerName := "kukusolutioncontroller"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller. revisionHistoryLimit:%d", meth, revisionHistoryLimit)

	syncedFunctions := make([]cache.InformerSynced, 10)
	syncedFunctions[0] = kukuSolutionInformer.Informer().HasSynced
	syncedFunctions[1] = v3DepInformer.Informer().HasSynced
	syncedFunctions[2] = kukuLinkInformer.Informer().HasSynced
	syncedFunctions[3] = revisionInformer.Informer().HasSynced
	syncedFunctions[4] = kukuSecretInformer.Informer().HasSynced
	syncedFunctions[5] = kukuVolumeInformer.Informer().HasSynced
	syncedFunctions[6] = kukuDomainInformer.Informer().HasSynced
	syncedFunctions[7] = kukuCertInformer.Informer().HasSynced
	syncedFunctions[8] = kukuCaInformer.Informer().HasSynced
	syncedFunctions[9] = kukuPortInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = kukuSolutionInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 2)
	secondarySharedInformers[0] = v3DepInformer.Informer()
	secondarySharedInformers[1] = kukuLinkInformer.Informer()
	// RevisionInformer is an special secondary resource
	// secondarySharedInformers[1] = revisionInformer.Informer()

	recorder := base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName)
	kukuSolutionLister := kukuSolutionInformer.Lister()
	resourceTools := resourcetools.NewResourceTools(
		kukuSecretInformer.Lister(),
		kukuVolumeInformer.Lister(),
		kukuDomainInformer.Lister(),
		kukuCertInformer.Lister(),
		kukuCaInformer.Lister(),
		kukuPortInformer.Lister(),
	)
	v3depLister := v3DepInformer.Lister()
	v3depTools := v3deptools.NewV3DepTools(kukuClientset, v3depLister, resourceTools, recorder)
	kukuLinkLister := kukuLinkInformer.Lister()
	kukuLinkTools := kukulinktools.NewKukuLinkTools(kukuClientset, kukuLinkLister, recorder)
	revisionsLister := revisionInformer.Lister()
	controllerHistory := history.NewHistory(kubeClientset, revisionInformer.Lister())

	queue := workqueue.NewNamedRateLimitingQueue(
		workqueue.DefaultControllerRateLimiter(),
		controllerName,
	)

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     common.KindKukuSolution,
		Workqueue:                queue,
		Recorder:                 recorder,
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}
	c = &KukuSolutionController{
		BaseController:       baseController,
		RevisionHistoryLimit: revisionHistoryLimit,
		kubeClientset:        kubeClientset,
		kukuClientset:        kukuClientset,
		kukuSolutionInformer: kukuSolutionInformer,
		kukuSolutionLister:   kukuSolutionLister,
		v3DepInformer:        v3DepInformer,
		v3DepLister:          v3depLister,
		v3DepTools:           v3depTools,
		kukuLinkInformer:     kukuLinkInformer,
		kukuLinkLister:       kukuLinkLister,
		kukuLinkTools:        kukuLinkTools,
		revisionInformer:     revisionInformer,
		revisionsLister:      revisionsLister,
		controllerHistory:    controllerHistory,
		resourceTools:        resourceTools,
	}
	// We use this "two-direction relationshipt" to implement abstract methods
	// See base/controller.go and this article:
	// https://medium.com/@adrianwit/abstract-class-reinvented-with-go-4a7326525034
	c.BaseController.IBaseController = c
	return
}

// HandleSecondary is in charge of processing changes in the secondary objects
// (kukulink, kukuhttpinbound, kukutcpinbound and v3dep) and force to re-evaluate
// the primary object (kukusolution)
// If the primary object is not retrieved (for example, due an error), it is not
// reeavaluated until other change occur.
//
// JJJ Realmente esto no va a ocurrir nunca, puesto que en este caso los secundarios
//     son objetos kuku que ningún proceso va a modificar... salvo que un humano
//     "les meta mano". Pero bueno, por supuesto esta función no molesta.
//
// JJJ TEMPORAMENTE ESTOY ASUMIENDO QUE, EN EL SECUNDARIO, OWNERREF=KUKUSOLUTION.
//     PERO NO QUEREMOS ESTO, QUEREMOS QUE HAYA UNA JERARQUÍA DE PADRES-HIJOS
//     ENTRE LOS DESPLIEGUES.
//     NOS VALE PARA ADELANTAR ESTA PRIMERA VERSIÓN DE PRUEBA.
//
//
func (c *KukuSolutionController) HandleSecondary(obj interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)

	object, kukuSolutionName, err := c.GetObject(obj)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}

	if kukuSolutionName != "" {
		log.Debugf("%s Processing object: %s", meth, object.GetName())
		ns := object.GetNamespace()
		kukuSolution, err := c.kukuSolutionLister.KukuSolutions(ns).Get(kukuSolutionName)

		if err != nil {
			if errors.IsNotFound(err) {
				log.Infof("%s ignoring orphaned object '%s' of KukuSolution '%s'",
					meth, object.GetSelfLink(), kukuSolutionName)
			} else {
				log.Warnf("%s error retrieving object %s: %v", meth, kukuSolutionName, err)
			}
			return
		}

		log.Debugf("%s enqueue %s", meth, kukuSolutionName)
		c.EnqueueObject(kukuSolution)
	}
}

// SyncHandler compares the actual state with the desired, and attempts to
// converge the two.
func (c *KukuSolutionController) SyncHandler(key string) error {
	meth := c.Name + ".SyncHandler() "
	log.Infof("%s %s", meth, key)

	// Errors have occurred that have not aborted the processing, but imply that
	// the primary object must be requeued at the end
	pendingRetryErrors := false

	// Retrieve kukusolution object
	kukuSolution, err := c.getKukuSolution(key)
	abort, errToReturn := kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// Check if KukuSolution is being deleted (if it has a deletion timestamp set)
	if !kukuSolution.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. KukuSolution has a deletion timestamp set. Skipping.", meth)
		return nil
	}

	// Updates the KukuSolution revisions. A new revision is created if this manifest
	// represetns a new version of the KukuSolution or restores a previous
	// version (i.e., it is a rollback).
	log.Debugf("%s. Updating revisions.", meth)
	err = c.UpdateRevisions(kukuSolution)
	if err != nil {
		log.Errorf("%s. Recoverable error found. Requeuing. Error: %v", meth, err)
		return err
	}
	log.Debugf("%s. Revisions updated", meth)

	// ---------------------------------------------------------------------------
	// For each secondary resource (v3dep, kukulink) related to the primary
	// (kukusolution), we get the list of items to be removed/updated/created
	// ---------------------------------------------------------------------------

	v3depsToBeRemoved, v3depsToBeUpdated, v3depsToBeCreated, v3depsDictionary, err :=
		c.getV3depChanges(kukuSolution, &pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	kukuLinksToBeRemoved, kukuLinksToBeUpdated, kukuLinksToBeCreated, err :=
		c.getKukuLinkChanges(kukuSolution, v3depsDictionary, &pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(v3depsToBeRemoved) == 0 &&
		len(v3depsToBeUpdated) == 0 &&
		len(v3depsToBeCreated) == 0 &&
		len(kukuLinksToBeRemoved) == 0 &&
		len(kukuLinksToBeUpdated) == 0 &&
		len(kukuLinksToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"v3deps(%d,%d,%d), kukuLinks(%d,%d,%d)",
			len(v3depsToBeRemoved),
			len(v3depsToBeUpdated),
			len(v3depsToBeCreated),
			len(kukuLinksToBeRemoved),
			len(kukuLinksToBeUpdated),
			len(kukuLinksToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (v3dep, kukulink) related to the primary
	// (kukusolution), we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncV3Dep(
		v3depsToBeRemoved,
		v3depsToBeUpdated,
		v3depsToBeCreated,
		kukuSolution,
		&pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	err = c.syncKukuLink(
		kukuLinksToBeRemoved,
		kukuLinksToBeUpdated,
		kukuLinksToBeCreated,
		kukuSolution,
		&pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// If an error has occurred during the process (without abort it), we return
	// an error so the kukusolution is requeued
	if pendingRetryErrors {
		return fmt.Errorf("non-critical errors ocurred processing %s", key)
	}

	return nil
}

// getKukuSolution retrieves a copy of the KukuSolution object from its workqueue key
func (c *KukuSolutionController) getKukuSolution(
	key string,
) (
	kukuSolution *kumoriv1.KukuSolution, kerr error,
) {
	meth := c.Name + ".getKukuCert() "

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("Invalid resource key: %s", key))
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	kukuSolution, err = c.kukuSolutionLister.KukuSolutions(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("'%s' in work queue no longer exists", key))
			log.Warnf("%s %s", meth, kerr.Error())
		} else {
			kerr = kerrors.NewKukuError(kerrors.Retry, err)
			log.Errorf("%s %s", meth, kerr.Error())
		}
		return
	}

	kukuSolution = kukuSolution.DeepCopy()
	return
}

// getV3depChanges returns the maps of v3dep to be removed, updated and created
// Maps are indexed by name
func (c *KukuSolutionController) getV3depChanges(
	kukuSolution *kumoriv1.KukuSolution,
	pendingRetryErrors *bool,
) (
	v3depsToBeRemoved map[string]*kumoriv1.V3Deployment,
	v3depsToBeUpdated map[string]*kumoriv1.V3Deployment,
	v3depsToBeCreated map[string]*kumoriv1.V3Deployment,
	v3depsDictionary map[string]string,
	errToReturn error,
) {
	meth := c.Name + ".getV3depChanges()"
	log.Infof(meth)

	v3depsToBeRemoved = make(map[string]*kumoriv1.V3Deployment)
	v3depsToBeUpdated = make(map[string]*kumoriv1.V3Deployment)
	v3depsToBeCreated = make(map[string]*kumoriv1.V3Deployment)
	v3depsDictionary = make(map[string]string)
	//v3depToBeUntouched := make(map[string]*kumoriv1.V3Deployment)

	// Get the current v3dep map (deployment name indexed) related to the solution
	v3depsCurrent, err := c.v3DepTools.GetV3DepsBySolution(kukuSolution)
	shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}
	log.Infof("%s Number of current v3deps: %d", meth, len(v3depsCurrent))

	// Calculate the v3dep map (deployment name indexed) that should exist related
	// to the solution.
	// NOTE: take into account that NEW internal names are generated for all v3dep,
	// including v3dep that already exists. Later, in this function, original
	// names are retrieved.
	v3depsNew, err := c.calculateV3DepsFromSolution(kukuSolution)
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}
	log.Infof("%s Number of calculated v3deps: %d", meth, len(v3depsNew))

	// V3Deps to be removed: deployments in the current list whose name doesn't appear
	// in the new list.
	for name, v3dep := range v3depsCurrent {
		_, found := v3depsNew[name]
		if !found {
			v3depsToBeRemoved[name] = v3dep
		}
	}

	// V3Deps to be created: deployments in the new list whose name doesn't appear
	// in the current list.
	for name, v3dep := range v3depsNew {
		_, found := v3depsCurrent[name]
		if !found {
			v3depsToBeCreated[name] = v3dep
			v3depsDictionary[name] = v3dep.GetName() // Map of names=>IDs
		}
	}

	// V3Deps to be updated: deployments names that appear in new and current list
	// but with different content.
	// The original v3dep is used as base, so its original internal name is used.
	// The original v3dep is not only modified with the new spec, but also its
	// labels and annotations are updated (see MergeToCurrent function)
	for name, v3depCurrent := range v3depsCurrent {
		v3depNew, found := v3depsNew[name]
		if found {
			v3depsDictionary[name] = v3depCurrent.GetName() // Map of names=>IDs
			if !c.v3DepTools.Equal(v3depCurrent, v3depNew) {
				v3depsToBeUpdated[name] = c.v3DepTools.MergeToCurrent(v3depCurrent, v3depNew, kukuSolution)
			}
			// } else {
			// 	v3depToBeUntouched[name] = v3depCurrent
			// }
		}
	}

	// Adjusts ownerRefs representing the topology of deployments/subdeployments.
	// If ownerref changes in a "v3depToBeUntouched" v3dep, move it to the update
	// list.
	//
	// JJJ TEMPORAMENTE NO HACEMOS ESTO. LO QUE HE HECHO ES QUE TODOS TENGAN
	//     COMO OWNERREF=KUKUSOLUTION (NO ES LO QUE QUEREMOS, PERO VALE PARA
	//     ADELANTAR ESTA PRIMERA VERSIÓN DE PRUEBA)
	//

	return
}

// getKukuLinkChanges returns the maps of kukulinks to be removed, updated and
// created. Maps are indexed by the kukulink ID (internal name)
func (c *KukuSolutionController) getKukuLinkChanges(
	kukuSolution *kumoriv1.KukuSolution,
	v3depsDictionary map[string]string, // Map of v3dep-Names => v3depIDs
	pendingRetryErrors *bool,
) (
	kukuLinksToBeRemoved []*kumoriv1.KukuLink,
	kukuLinksToBeUpdated []*kumoriv1.KukuLink,
	kukuLinksToBeCreated []*kumoriv1.KukuLink,
	errToReturn error,
) {
	meth := c.Name + ".getKukuLinkChanges()"
	log.Infof(meth)
	kukuLinksToBeRemoved = make([]*kumoriv1.KukuLink, 0)
	kukuLinksToBeUpdated = make([]*kumoriv1.KukuLink, 0)
	kukuLinksToBeCreated = make([]*kumoriv1.KukuLink, 0)

	// Get the current kukulink list related to the solution
	kukuLinksCurrent, err := c.kukuLinkTools.GetKukuLinksBySolution(kukuSolution)
	shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}
	log.Infof("%s Number of current kukulinks: %d", meth, len(kukuLinksCurrent))

	// Calculate the kukulink list that should exist related to the solution.
	kukuLinksNew, err := c.calculateKukuLinksFromSolution(kukuSolution, v3depsDictionary)
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}
	log.Infof("%s Number of calculated kukulinks: %d", meth, len(kukuLinksNew))

	// KukuLinks to be removed: appears in the current list but doesnt appear in
	// new list
	for _, kukuLink := range kukuLinksCurrent {
		if !c.kukuLinkTools.Search(kukuLink, kukuLinksNew) {
			kukuLinksToBeRemoved = append(kukuLinksToBeRemoved, kukuLink)
		}
	}

	// KukuLinks to be added: appears in the new list but doesnt appear in
	// current list
	for _, kukuLink := range kukuLinksNew {
		if !c.kukuLinkTools.Search(kukuLink, kukuLinksCurrent) {
			kukuLinksToBeCreated = append(kukuLinksToBeCreated, kukuLink)
		}
	}

	// KukuLinks to be updates: none. KukuLinks are never updated.

	return
}

// syncV3Dep checks if the the v3dep objects related to the kukusolution
// exactly exists, creating/updating/removing v3dep objects received as parameters
func (c *KukuSolutionController) syncV3Dep(
	v3depsToBeRemoved map[string]*kumoriv1.V3Deployment,
	v3depsToBeUpdated map[string]*kumoriv1.V3Deployment,
	v3depsToBeCreated map[string]*kumoriv1.V3Deployment,
	kukuSolution *kumoriv1.KukuSolution,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncV3Dep()"
	log.Infof(meth)

	err := c.v3DepTools.ExecuteActionInMap(
		common.ActionDelete,
		v3depsToBeRemoved,
		kukuSolution,
		pendingRetryErrors,
	)
	shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	err = c.v3DepTools.ExecuteActionInMap(
		common.ActionUpdate,
		v3depsToBeUpdated,
		kukuSolution,
		pendingRetryErrors,
	)
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	err = c.v3DepTools.ExecuteActionInMap(
		common.ActionCreate,
		v3depsToBeCreated,
		kukuSolution,
		pendingRetryErrors,
	)
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	return
}

// syncKukuLink checks if the the kukulink objects related to the kukusolution
// exactly exists, creating/updating/removing kukulink objects received as parameters
func (c *KukuSolutionController) syncKukuLink(
	kukuLinksToBeRemoved []*kumoriv1.KukuLink,
	kukuLinksToBeUpdated []*kumoriv1.KukuLink,
	kukuLinksToBeCreated []*kumoriv1.KukuLink,
	kukuSolution *kumoriv1.KukuSolution,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncKukuLink()"
	log.Infof(meth)

	err := c.kukuLinkTools.ExecuteActionInMap(
		common.ActionDelete,
		kukuLinksToBeRemoved,
		kukuSolution,
		pendingRetryErrors,
	)
	shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	err = c.kukuLinkTools.ExecuteActionInMap(
		common.ActionUpdate,
		kukuLinksToBeUpdated,
		kukuSolution,
		pendingRetryErrors,
	)
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	err = c.kukuLinkTools.ExecuteActionInMap(
		common.ActionCreate,
		kukuLinksToBeCreated,
		kukuSolution,
		pendingRetryErrors,
	)
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	return
}

// calculateV3DepsFromSolution calculates the v3dep map that should exist related to
// the solution. The map is indexed by deployment name (not its internal name).
//
// JJJ TEMPORAMENTE HACEMOS QUE TODOS LOS V3DEP TENGAN COMO OWNERREF=KUKUSOLUTION.
//     NO ES LO QUE QUEREMOS, PERO VALE PARA ADELANTAR ESTA PRIMERA VERSIÓN DE PRUEBA)
//
func (c *KukuSolutionController) calculateV3DepsFromSolution(
	kukuSolution *kumoriv1.KukuSolution,
) (
	map[string]*kumoriv1.V3Deployment, error,
) {
	meth := c.Name + ".calculateV3DepsFromSolution()"
	log.Infof("%s solution-id: %s", meth, kukuSolution.GetName())

	v3depsMap := make(map[string]*kumoriv1.V3Deployment)
	for _, v3depSpec := range kukuSolution.Spec.Deployments {
		v3dep, v3depName, err := c.v3DepTools.GenerateV3Dep(v3depSpec, kukuSolution)
		if err != nil {
			return nil, err
		}
		v3depsMap[v3depName] = v3dep
	}

	return v3depsMap, nil
}

// calculateKukuLinksFromSolution calculates the kukulink list that should exist
// related to the solution.
//
// BE CAREFUL! it is not the same kumoriv1.KukuLink that kumoriv1.Link
//
func (c *KukuSolutionController) calculateKukuLinksFromSolution(
	kukuSolution *kumoriv1.KukuSolution,
	v3depsDictionary map[string]string, // Map of v3dep-Names => v3depIDs
) (
	[]*kumoriv1.KukuLink, error,
) {
	meth := c.Name + ".calculateKukuLinksFromSolution()"
	log.Infof("%s solution-id: %s", meth, kukuSolution.GetName())
	kukuLinks := make([]*kumoriv1.KukuLink, 0)
	for _, solutionLink := range kukuSolution.Spec.Links {
		kukuLink, err := c.kukuLinkTools.GenerateKukuLink(solutionLink, kukuSolution, v3depsDictionary)
		if err != nil {
			return nil, kerrors.NewKukuError(kerrors.Abort, err)
		}
		kukuLinks = append(kukuLinks, kukuLink)
	}
	return kukuLinks, nil
}
