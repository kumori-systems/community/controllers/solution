/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package helper

import (
	"bytes"
	"compress/gzip"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"strings"
	"time"
)

// GetRandomValueHex returns an hexadecimal random value as string, with a
// provided sizevaluerandom string
func GetRandomValueHex(size int) string {
	bytesSize := size / 2
	bytes := make([]byte, bytesSize)
	rand.Read(bytes)
	return hex.EncodeToString(bytes)
}

// GetHHMMSS return current time in HHMMSS format
func GetHHMMSS() string {
	return time.Now().Format("150405")
}

// GetUTCDateTime return current UTC datetime in the format used by
// Kubernetes for annotations and labels
func GetUTCDateTime() string {
	return time.Now().UTC().Format("2006-01-02T15:04:05Z")
}

// CalculateSha calculates the SHA1 value of an array of bytes, and returns
// the string of its hex format
func CalculateSha(source []byte) string {
	h := sha1.New()
	h.Write(source)
	bs := h.Sum(nil)
	return fmt.Sprintf("%x", bs)
}

// GzipCompress gzips a []byte source and returns the []byte result
func GzipCompress(source []byte) ([]byte, error) {
	var destination bytes.Buffer
	gz := gzip.NewWriter(&destination)
	if _, err := gz.Write(source); err != nil {
		return nil, err
	}
	if err := gz.Close(); err != nil {
		return nil, err
	}
	return destination.Bytes(), nil
}

// Base64Encode returns the encoded string of a []bytes source
func Base64Encode(source []byte) string {
	return base64.StdEncoding.EncodeToString(source)
}

// UnscapeOwnerField converts the owner stored in labels, formatted as
// "juanjo__arroba__kumori.cloud" to something like "juanjo@kumori.cloud"
// (Kubernetes labels restricts the format of its values)
func UnscapeOwnerField(scapedOwner string) (unescapedOwner string) {
	return strings.Replace(scapedOwner, "__arroba__", "@", 1)
}
