/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	helper "solution-controller/pkg/utils/helper"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

func CalculateV3DepId(ksolname string, v3DepSpec kumoriv1.V3DeploymentSpec) string {
	// The V3Deployment ID is deterministically as "kd-<TIMESTAMP>-<HASH>" where:
	// - TIMESTAMP: is the timestamp in the KukuSolution ID (Example: if the KukuSolution ID is
	//   "ks-082834-a0f3c663" the timestamp is "082834")
	// - A hash calculated from the concatenation of the KukuSolution ID and the deployment name
	//   as declared in the KukusSlution spec (Example: hash("ks-082834-a0f3c663/helloworlddep"))
	timestamp := ksolname[3:9]
	hashedname := Hash(fmt.Sprintf("%s/%s", ksolname, v3DepSpec.Name))
	return fmt.Sprintf("kd-%s-%s", timestamp, hashedname)

	// This is how it was calculated in previous versions
	// - Version 1:
	// return "kd-" + helper.GetHHMMSS() + "-" + helper.GetRandomValueHex(8)
	// - Version 2:
	// return kukuSolution.GetName() + "-" + deploymentName
}

func CalculateKukuLinkId(kukuLinkSpec kumoriv1.KukuLinkSpec) (string, error) {
	//return "kl-" + helper.GetHHMMSS() + "-" + helper.GetRandomValueHex(8)
	jsonSolutionLink, err := json.Marshal(kukuLinkSpec)
	if err != nil {
		return "", err
	}
	return "kl-" + helper.CalculateSha(jsonSolutionLink), nil
}

// Hash returns a hashed version of a given string
func Hash(source string) string {
	hf := fnv.New32()
	hf.Write([]byte(source))
	return string(fmt.Sprintf("%08x", hf.Sum32()))
	// return rand.SafeEncodeString(fmt.Sprint(hf.Sum32()))
}
