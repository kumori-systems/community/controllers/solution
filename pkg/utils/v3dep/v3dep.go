/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v3dep

import (
	"context"
	"encoding/json"
	"fmt"

	common "solution-controller/pkg/utils/common"
	kerrors "solution-controller/pkg/utils/errors"
	helper "solution-controller/pkg/utils/helper"
	naming "solution-controller/pkg/utils/naming"
	resourcetools "solution-controller/pkg/utils/resources"

	log "github.com/sirupsen/logrus"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kukulisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	corev1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	record "k8s.io/client-go/tools/record"
)

// V3DeploymentSpecPlus is the same type as V3DeploymentSpec, but adding
// some fields required to create the manifest to be included as annotation
type V3DeploymentSpecPlus struct {
	kumoriv1.V3DeploymentSpec
	Comment           string `json:"comment"`
	Owner             string `json:"owner"`
	CreationTimestamp string `json:"creationTimestamp"`
	LastModification  string `json:"lastModification"`
	URN               string `json:"urn"`
	Name              string `json:"name"`
}

// V3DepTools includes functions related to kube v3deps
type V3DepTools struct {
	clientset     kukuclientset.Interface
	lister        kukulisters.V3DeploymentLister
	resourceTools resourcetools.ResourceTools
	recorder      record.EventRecorder
}

// NewV3DepTools returns a new V3DepTools
func NewV3DepTools(
	clientset kukuclientset.Interface,
	lister kukulisters.V3DeploymentLister,
	resourceTools resourcetools.ResourceTools,
	recorder record.EventRecorder,
) (t V3DepTools) {
	t = V3DepTools{
		clientset:     clientset,
		lister:        lister,
		resourceTools: resourceTools,
		recorder:      recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of v3deps.
// If action is 'delete', a deleteOptions can be provided
func (t V3DepTools) ExecuteActionInMap(
	action string,
	v3deps map[string]*kumoriv1.V3Deployment,
	kukuSolution *kumoriv1.KukuSolution,
	pendingRetryErrors *bool,
) (kerr error) {
	meth := "V3DepTools.ExecuteActionInMap()"

	if v3deps == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "V3Deps is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range v3deps {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, kukuSolution)
		case common.ActionUpdate:
			err = t.Update(v, kukuSolution)
		case common.ActionDelete:
			err = t.Delete(v, kukuSolution)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if shouldAbort {
			return errToReturn
		}
	}

	return
}

// Create adds the v3dep
func (t V3DepTools) Create(
	v3dep *kumoriv1.V3Deployment,
	kukuSolution *kumoriv1.KukuSolution,
) (kerr error) {
	meth := "V3DepTools.Create()"

	if v3dep == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "V3Deployment is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := v3dep.GetName()
	ns := v3dep.GetNamespace()
	log.Infof("%s v3dep %s to be added", meth, name)

	// TODO JJJ
	// Esto no lo había tenido que usar hasta ahora! comprobar si algo que quiero
	// crear ya existe ...
	found, err := t.Exist(v3dep)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s V3Deployment %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: common.FieldManager,
	}
	v3dep, err = t.clientset.KumoriV1().V3Deployments(ns).Create(ctx, v3dep, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "V3Deployment", v3dep.GetName())
	t.recorder.Event(kukuSolution, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s V3Deployment %s created", meth, name)
	return
}

// Update updates the v3dep
func (t V3DepTools) Update(
	v3dep *kumoriv1.V3Deployment,
	kukuSolution *kumoriv1.KukuSolution,
) (kerr error) {
	meth := "V3DepTools.Update()"

	if v3dep == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "V3Deployment is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := v3dep.GetName()
	ns := v3dep.GetNamespace()
	log.Infof("%s v3dep %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: common.FieldManager,
	}
	v3dep, err := t.clientset.KumoriV1().V3Deployments(ns).Update(ctx, v3dep, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "V3Deployment", v3dep.GetName())
	t.recorder.Event(kukuSolution, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s V3Deployment %s updated", meth, name)

	return
}

// Delete removes the v3dep
func (t V3DepTools) Delete(
	v3dep *kumoriv1.V3Deployment,
	kukuSolution *kumoriv1.KukuSolution,
) (kerr error) {
	meth := "V3DepTools.Delete()"

	if v3dep == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "V3Deployment is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := v3dep.GetName()
	ns := v3dep.GetNamespace()
	log.Infof("%s v3dep %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.KumoriV1().V3Deployments(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "V3Deployment", v3dep.GetName())
	t.recorder.Event(kukuSolution, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s V3Deployment %s removed", meth, name)

	return
}

// Exists checks if the v3dep already exists
func (t V3DepTools) Exist(v3dep *kumoriv1.V3Deployment) (bool, error) {
	meth := "V3DepTools.Exist()"
	if v3dep == nil {
		return false, nil
	}

	matchLabels := map[string]string{
		common.V3depOwnerLabel:  v3dep.GetLabels()[common.V3depOwnerLabel],
		common.V3depDomainLabel: v3dep.GetLabels()[common.V3depDomainLabel],
		common.V3depNameLabel:   v3dep.GetLabels()[common.V3depNameLabel],
	}
	labelSelector := labels.SelectorFromSet(matchLabels)
	v3deps, err := t.lister.V3Deployments(v3dep.GetNamespace()).List(labelSelector)

	if (err != nil) && !errors.IsNotFound(err) {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if errors.IsNotFound(err) || v3deps == nil || len(v3deps) <= 0 {
		return false, nil
	}
	return true, nil
}

// GetV3DepsBySolution returns the map of v3deps related to a kukusolution,
// indexed by deployment name (not its internal name)
func (t *V3DepTools) GetV3DepsBySolution(
	kukuSolution *kumoriv1.KukuSolution,
) (
	v3deps map[string]*kumoriv1.V3Deployment,
	err error,
) {
	meth := "V3DepTools.GetV3DepsBySolution()"
	v3deps = make(map[string]*kumoriv1.V3Deployment)

	solutionId := kukuSolution.GetName()
	solutionNs := kukuSolution.GetNamespace()
	matchLabels := map[string]string{common.V3depSolutionIdLabel: solutionId}

	labelSelector := labels.SelectorFromSet(matchLabels)
	v3depsList, err := t.lister.V3Deployments(solutionNs).List(labelSelector)
	if errors.IsNotFound(err) || v3depsList == nil || len(v3depsList) <= 0 {
		err = nil
	} else if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		err = kerrors.NewKukuError(kerrors.Retry, err)
	} else {
		for _, v3dep := range v3depsList {
			name := v3dep.GetAnnotations()[common.V3depNameAnnotation]
			v3deps[name] = v3dep.DeepCopy()
		}
	}

	return
}

// MergeToCurrent updates the current V3Dep with properties of the new V3Dep.
// Which parts have changed?
// - The spec (obvious)
// - The annotations, that depends on the spec (hash, manifest) and the user
//   operation (lastModification, comment)
func (t V3DepTools) MergeToCurrent(
	currentobj *kumoriv1.V3Deployment,
	newobj *kumoriv1.V3Deployment,
	kukuSolution *kumoriv1.KukuSolution,
) *kumoriv1.V3Deployment {

	// Should be copied all the spec
	currentobj.Spec = *newobj.Spec.DeepCopy()

	// Should be copied all the annotations, or only the identified annotations?
	// (comment, lastModification, hash, manifest). It is not clear for me ...
	// TODO: UGLY IMPLEMENTATION
	// The manifest of newobj contains the ID of the new v3dep (obviously)
	// But, in the merge, we want to keep the old ID in the manifest of currentobj.
	// So... the manifest must be regenerated
	currentobj.Annotations = make(map[string]string)
	for key, value := range newobj.Annotations {
		if key != common.V3depManifestAnnotation && key != common.V3depShaAnnotation {
			currentobj.Annotations[key] = value
		}
	}
	sha, manifest, _ := t.getShaAndEncodedManifest( // Error is not posible! (executed earlier)
		currentobj.GetName(),
		currentobj.Spec,
		kukuSolution,
		helper.GetUTCDateTime(),
	)
	currentobj.Annotations[common.V3depShaAnnotation] = sha
	currentobj.Annotations[common.V3depManifestAnnotation] = manifest

	// Should be copied all the labels, or only the identified labels?
	// It is not clear for me ...
	currentobj.Labels = make(map[string]string)
	for key, value := range newobj.Labels {
		currentobj.Labels[key] = value
	}

	return currentobj
}

// Equal returns true if two V3Dep are equivalent
func (t V3DepTools) Equal(
	o1, o2 *kumoriv1.V3Deployment,
) bool {

	if o1 == nil && o2 == nil {
		return true
	}
	if o1 == nil && o2 != nil {
		return false
	}
	if o1 != nil && o2 == nil {
		return false
	}

	// Compare specs
	isEqualSpec := apiequality.Semantic.DeepEqual(o1.Spec, o2.Spec)

	// Compare labels and annotations created when a new v3dep is generated
	isEqualLabelsAndAnnotations := (o1.GetLabels()["V3depNameLabel"] == o2.GetLabels()["V3depNameLabel"]) &&
		(o1.GetLabels()["V3depOwnerLabel"] == o2.GetLabels()["V3depOwnerLabel"]) &&
		(o1.GetLabels()["V3depSolutionIdLabel"] == o2.GetLabels()["V3depSolutionIdLabel"]) &&
		(o1.GetLabels()["V3depSolutionDomainLabel"] == o2.GetLabels()["V3depSolutionDomainLabel"]) &&
		(o1.GetLabels()["V3depSolutionNameLabel"] == o2.GetLabels()["V3depSolutionNameLabel"]) &&
		(o1.GetLabels()["V3depDomainLabel"] == o2.GetLabels()["V3depDomainLabel"]) &&
		(o1.GetAnnotations()["V3depCommentAnnotation"] == o2.GetAnnotations()["V3depCommentAnnotation"]) &&
		(o1.GetAnnotations()["V3depLastModificationAnnotation"] == o2.GetAnnotations()["V3depLastModificationAnnotation"]) &&
		(o1.GetAnnotations()["V3depShaAnnotation"] == o2.GetAnnotations()["V3depShaAnnotation"]) &&
		(o1.GetAnnotations()["V3depManifestAnnotation"] == o2.GetAnnotations()["V3depManifestAnnotation"]) &&
		(o1.GetAnnotations()["V3depOwnerAnnotation"] == o2.GetAnnotations()["V3depOwnerAnnotation"]) &&
		(o1.GetAnnotations()["V3depSolutionDomainAnnotation"] == o2.GetAnnotations()["V3depSolutionDomainAnnotation"]) &&
		(o1.GetAnnotations()["V3depSolutionNameAnnotation"] == o2.GetAnnotations()["V3depSolutionNameAnnotation"]) &&
		(o1.GetAnnotations()["V3depNameAnnotation"] == o2.GetAnnotations()["V3depNameAnnotation"]) &&
		(o1.GetAnnotations()["V3depDomainAnnotation"] == o2.GetAnnotations()["V3depDomainAnnotation"])

	// Special labels: resources-in-use (the key of the label is the part really significant)
	isEqualResourcesInUse := true
	for k, v := range o1.GetLabels() {
		if v == common.ResourceInUseLabel {
			if o2.GetLabels()[k] != v {
				isEqualResourcesInUse = false
				break
			}
		}
	}
	for k, v := range o2.GetLabels() {
		if v == common.ResourceInUseLabel {
			if o1.GetLabels()[k] != v {
				isEqualResourcesInUse = false
				break
			}
		}
	}

	return (isEqualSpec && isEqualLabelsAndAnnotations && isEqualResourcesInUse)
}

// GenerateV3Dep created a V3Deployment object, from provided spec, and
// related solution object
//
// JJJ TEMPORAMENTE HACEMOS QUE TODOS LOS V3DEP TENGAN COMO OWNERREF=KUKUSOLUTION.
//     NO ES LO QUE QUEREMOS, PERO VALE PARA ADELANTAR ESTA PRIMERA VERSIÓN DE PRUEBA)
//
// Example:
//
// apiVersion: kumori.systems/v1
// kind: V3Deployment
// metadata:
//   name: kd-hhmmss-12345678 // 12345678=random
//   namespace: kumori (same as kukusolution)
//   ownerref: ks-192120-eadc7498  // If root deployment
//             kd-hhmmss-11111111  // If subservice
//   creationTimestamp: "2022-01-25T08:47:35Z" // Generated by kubernetes
//   annotations:
//     kumori/comment: my comment
//     kumori/lastModification: "2022-01-25T08:47:35Z"
//     kumori/hash: 222222 // Calculated from spec
//     kumori/manifest: [...] // Original manifest where several fiels are
//                            // added (comment, owner, creationTimestamp,
//                            // lastModification, internal name and the URN),
//                            // and then is gzipped and base64-endoded.
//   labels:
//     kumori/owner: juanjo__arroba__kumori.cloud
//     kumori/solution.id: ks-192120-eadc7498
//     kumori/solution.domain: default.domain
//     kumori/solution.name: mydeployment
//     kumori/name: mydeployment // Or mysubdeployment, if is a subservice
//     kumori/domain: default
// spec:
//   config: { .... }
//   name: mydeployment // Or mysubdeployment, if is a subservice
//   meta: { ... }
//   up: null // Or mydeployment, if it is a subservice
//   artifact: { ... }
//
func (t *V3DepTools) GenerateV3Dep(
	v3DepSpec kumoriv1.V3DeploymentSpec,
	kukuSolution *kumoriv1.KukuSolution,
) (
	v3Dep *kumoriv1.V3Deployment, v3DepName string, err error,
) {
	meth := "V3DepTools.GenerateV3Dep()"
	v3DepName = v3DepSpec.Name
	v3DepID := naming.CalculateV3DepId(kukuSolution.Name, v3DepSpec)
	log.Infof("%s %s (%s)", meth, v3DepID, v3DepName)

	namespace := kukuSolution.GetNamespace()
	owner := kukuSolution.GetAnnotations()[common.KukuSolutionOwnerAnnotation]
	domain := kukuSolution.GetAnnotations()[common.KukuSolutionDomainAnnotation]
	utcDateTime := helper.GetUTCDateTime()

	// From the spec:
	// - calculate a hash (sha1) of its content,
	// - get its manifest: the base64-encoded of the compressed (gzip) of the JSON
	//   version of the spec, but adding first several fields (comment, owner, name,
	//   creationTimeStamp, lastModification)
	sha, manifest, err := t.getShaAndEncodedManifest(v3DepID, v3DepSpec, kukuSolution, utcDateTime)
	if err != nil {
		err = kerrors.NewKukuError(kerrors.Abort, err)
		return
	}

	// Annotations
	annotations := make(map[string]string)
	annotations[common.V3depCommentAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionCommentAnnotation]
	annotations[common.V3depLastModificationAnnotation] = utcDateTime
	annotations[common.V3depShaAnnotation] = sha
	annotations[common.V3depManifestAnnotation] = manifest
	annotations[common.V3depOwnerAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionOwnerAnnotation]
	annotations[common.V3depSolutionDomainAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionDomainAnnotation]
	annotations[common.V3depSolutionNameAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionNameAnnotation]
	annotations[common.V3depNameAnnotation] = v3DepName
	annotations[common.V3depDomainAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionDomainAnnotation]

	// Labels
	labels := make(map[string]string)
	labels[common.V3depNameLabel] = naming.Hash(v3DepName)
	labels[common.V3depOwnerLabel] = kukuSolution.GetLabels()[common.KukuSolutionOwnerLabel]
	labels[common.V3depSolutionIdLabel] = kukuSolution.GetName()
	labels[common.V3depSolutionDomainLabel] = kukuSolution.GetLabels()[common.KukuSolutionDomainLabel]
	labels[common.V3depSolutionNameLabel] = kukuSolution.GetLabels()[common.KukuSolutionNameLabel]
	labels[common.V3depDomainLabel] = kukuSolution.GetLabels()[common.KukuSolutionDomainLabel]

	// Labels related to resources in use.
	// Resources must be searched in the "leaves on the tree": that is, in role
	// artifacts and builtin services.
	serviceDescription := *v3DepSpec.Artifact.Description
	if serviceDescription.Builtin {
		for _, resource := range *serviceDescription.Config.Resource {
			resID, resErr := t.resourceTools.GetResourceID(namespace, owner, domain, resource)
			if err != nil {
				err = kerrors.NewKukuError(kerrors.Retry, resErr)
				return
			} else if resID != "" {
				labels[resID] = common.ResourceInUseLabel
			}
		}
	} else {
		for _, role := range *serviceDescription.Roles {
			roleConfig := role.Artifact.Description.Config
			for _, resource := range *roleConfig.Resource {
				resID, resErr := t.resourceTools.GetResourceID(namespace, owner, domain, resource)
				if err != nil {
					err = kerrors.NewKukuError(kerrors.Retry, resErr)
					return
				} else if resID != "" {
					labels[resID] = common.ResourceInUseLabel
				}
			}
		}
	}

	// Metadata
	meta := metav1.ObjectMeta{
		Name:      v3DepID,
		Namespace: namespace,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(kukuSolution, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindKukuSolution,
			}),
		},
	}
	meta.SetLabels(labels)
	meta.SetAnnotations(annotations)

	v3Dep = &kumoriv1.V3Deployment{
		ObjectMeta: meta,
		Spec:       v3DepSpec,
	}

	return
}

// getShaAndEncodedManifest calculates, from the spec:
// - calculate a hash (sha1) of its content,
// - get its manifest: the base64-encoded of the compressed (gzip) of the JSON
//   version of the spec, but adding first several fields (comment, owner,
//   creationTimeStamp, lastModification, internal name and urn)
func (t *V3DepTools) getShaAndEncodedManifest(
	v3DepID string,
	v3DepSpec kumoriv1.V3DeploymentSpec,
	kukuSolution *kumoriv1.KukuSolution,
	utcDateTime string,
) (
	hash string, encodedManifest string, err error,
) {

	comment := kukuSolution.GetAnnotations()[common.V3depCommentAnnotation]
	owner := kukuSolution.GetAnnotations()[common.V3depOwnerAnnotation]

	creationTimeStamp := utcDateTime
	lastModification := utcDateTime
	urn :=
		"eslap://" +
			kukuSolution.GetAnnotations()[common.KukuSolutionDomainAnnotation] +
			"/deployment/" +
			v3DepSpec.Name

	// Calculate the hash of the spec
	jsonV3DepSpec, err := json.Marshal(v3DepSpec)
	if err != nil {
		return
	}
	hash = helper.CalculateSha(jsonV3DepSpec)

	// Add new fields to the original spec
	v3DepSpecPlus := V3DeploymentSpecPlus{
		V3DeploymentSpec:  v3DepSpec,
		Comment:           comment,
		Owner:             helper.UnscapeOwnerField(owner),
		CreationTimestamp: creationTimeStamp,
		LastModification:  lastModification,
		URN:               urn,
		Name:              v3DepID,
	}
	jsonV3DepSpecPlus, err := json.Marshal(v3DepSpecPlus)
	if err != nil {
		return
	}
	gzipV3DepSpecPlus, err := helper.GzipCompress(jsonV3DepSpecPlus)
	if err != nil {
		return
	}
	encodedManifest = helper.Base64Encode(gzipV3DepSpecPlus)

	return

}
