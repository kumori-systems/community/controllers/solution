/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package kukulink

import (
	"context"
	"encoding/json"
	"fmt"

	common "solution-controller/pkg/utils/common"
	kerrors "solution-controller/pkg/utils/errors"
	helper "solution-controller/pkg/utils/helper"
	naming "solution-controller/pkg/utils/naming"

	log "github.com/sirupsen/logrus"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kukulisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	corev1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	record "k8s.io/client-go/tools/record"
)

// LinkPlus is the same type as V3DeploymentSpec, but adding
// some fields required to create the manifest to be included as annotation
type LinkPlus struct {
	kumoriv1.Link
	Name  string `json:"name"`
	Owner string `json:"owner"`
}

// KukuLinkTools includes functions related to kube v3deps
type KukuLinkTools struct {
	clientset kukuclientset.Interface
	lister    kukulisters.KukuLinkLister
	recorder  record.EventRecorder
}

// NewKukuLinkTools returns a new KukuLinkTools
func NewKukuLinkTools(
	clientset kukuclientset.Interface,
	lister kukulisters.KukuLinkLister,
	recorder record.EventRecorder,
) (t KukuLinkTools) {
	t = KukuLinkTools{
		clientset: clientset,
		lister:    lister,
		recorder:  recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of v3deps.
// If action is 'delete', a deleteOptions can be provided
func (t KukuLinkTools) ExecuteActionInMap(
	action string,
	kukuLinks []*kumoriv1.KukuLink,
	kukuSolution *kumoriv1.KukuSolution,
	pendingRetryErrors *bool,
) (kerr error) {
	meth := "KukuLinkTools.ExecuteActionInMap()"

	if kukuLinks == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "KukuLinks is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range kukuLinks {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, kukuSolution)
		case common.ActionUpdate:
			err = t.Update(v, kukuSolution)
		case common.ActionDelete:
			err = t.Delete(v, kukuSolution)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if shouldAbort {
			return errToReturn
		}
	}

	return
}

// Create adds the kukulink
func (t KukuLinkTools) Create(
	kukuLink *kumoriv1.KukuLink,
	kukuSolution *kumoriv1.KukuSolution,
) (kerr error) {
	meth := "KukuLinkTools.Create()"

	if kukuLink == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "KukuLink is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := kukuLink.GetName()
	ns := kukuLink.GetNamespace()
	log.Infof("%s kukuLink %s to be added", meth, name)

	// TODO JJJ
	// Esto no lo había tenido que usar hasta ahora! comprobar si algo que quiero
	// crear ya existe ...
	found, err := t.Exist(kukuLink)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s KukuLink %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: common.FieldManager,
	}
	kukuLink, err = t.clientset.KumoriV1().KukuLinks(ns).Create(ctx, kukuLink, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "KukuLink", kukuLink.GetName())
	t.recorder.Event(kukuSolution, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s KukuLink %s created", meth, name)
	return
}

// Update updates the kukulink
func (t KukuLinkTools) Update(
	kukuLink *kumoriv1.KukuLink,
	kukuSolution *kumoriv1.KukuSolution,
) (kerr error) {
	meth := "KukuLinkTools.Update()"

	if kukuLink == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "KukuLink is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := kukuLink.GetName()
	ns := kukuLink.GetNamespace()
	log.Infof("%s KukuLink %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: common.FieldManager,
	}
	kukuLink, err := t.clientset.KumoriV1().KukuLinks(ns).Update(ctx, kukuLink, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "KukuLink", kukuLink.GetName())
	t.recorder.Event(kukuSolution, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s KukuLink %s updated", meth, name)

	return
}

// Delete removes the kukulink
func (t KukuLinkTools) Delete(
	kukuLink *kumoriv1.KukuLink,
	kukuSolution *kumoriv1.KukuSolution,
) (kerr error) {
	meth := "KukuLinkTools.Delete()"

	if kukuLink == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "KukuLink is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := kukuLink.GetName()
	ns := kukuLink.GetNamespace()
	log.Infof("%s v3dep %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.KumoriV1().KukuLinks(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "KukuLink", kukuLink.GetName())
	t.recorder.Event(kukuSolution, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s KukuLink %s removed", meth, name)

	return
}

// Exists checks if the KukuLink already exists
func (t KukuLinkTools) Exist(kukuLink *kumoriv1.KukuLink) (bool, error) {
	meth := "KukuLinkTools.Exist()"
	if kukuLink == nil {
		return false, nil
	}

	name := kukuLink.GetName()
	ns := kukuLink.GetNamespace()
	foundKukuLink, err := t.lister.KukuLinks(ns).Get(name)

	if (err != nil) && !errors.IsNotFound(err) {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if errors.IsNotFound(err) || foundKukuLink == nil {
		return false, nil
	}
	return true, nil
}

// GetKukuLinksBySolution returns the list of kukulinks related to a kukusolution,
func (t *KukuLinkTools) GetKukuLinksBySolution(
	kukuSolution *kumoriv1.KukuSolution,
) (
	kukulinks []*kumoriv1.KukuLink,
	err error,
) {
	meth := "KukuLinkTools.GetV3DepsBySolution()"

	solutionId := kukuSolution.GetName()
	solutionNs := kukuSolution.GetNamespace()
	matchLabels := map[string]string{common.KukuLinkSolutionIdLabel: solutionId}

	labelSelector := labels.SelectorFromSet(matchLabels)
	kukulinks, err = t.lister.KukuLinks(solutionNs).List(labelSelector)
	if errors.IsNotFound(err) || kukulinks == nil || len(kukulinks) <= 0 {
		err = nil
	} else if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		err = kerrors.NewKukuError(kerrors.Retry, err)
	}

	return
}

// GenerateKukuLink created a KukuLink object, from provided Solution-Link, and
// related solution object
//
// JJJ TODO: por ahora estamos utilizando el "viejo" formato
//
// Example:
//
// apiVersion: kumori.systems/v1
// kind: KukuLink
// metadata:
//
//	name: kl-12345678 // 12345678=hash(kukulink.spec)
//	namespace: kumori
//	annotations:
//	  kumori/hash: 222222 // Of spec
//	  kumori/manifest: [...]  // Spec gzipped and base64-encoded
//	labels:
//	  kumori/owner: juanjo__arroba__kumori.cloud
//	  kumori/solution.id: ks-HHMMSS-87654321
//	  kumori/solution.name: myapplication
//	  kumori/solution.domain: default
//	  channel1: mychan1
//	  channel2: mychan2
//	  deployment1: kd-135507-11ecba41
//	  deployment2: kd-135507-603f94cf
//	  kd-135507-11ecba41: deployment1
//	  kd-135507-603f94cf: deployment2
//
// spec:
//
//	endpoints:
//	- channel: mychan1
//	  domain: my.examples
//	  kind: deployment
//	  name: mydep1
//	- channel: mychan2
//	  domain: my.examples
//	  kind: deployment
//	  name: mydep2
func (t *KukuLinkTools) GenerateKukuLink(
	solutionLink kumoriv1.Link,
	kukuSolution *kumoriv1.KukuSolution,
	v3depsDictionary map[string]string, // Map of v3dep-Names=>v3depIDs
) (
	kukuLink *kumoriv1.KukuLink, err error,
) {
	meth := "KukuLinkTools.GenerateKukuLink()"

	// First of all, prepare the spec of the kukulink (will be used to compose
	// generate the ID of the kukulink)
	domain := kukuSolution.GetAnnotations()[common.KukuSolutionDomainAnnotation]
	endPoints := make([]kumoriv1.KukuEndpoint, 0)
	endPoints = append(endPoints, kumoriv1.KukuEndpoint{
		Kind:    "deployment",
		Domain:  domain,
		Name:    solutionLink.SourceDeployment,
		Channel: solutionLink.SourceChannel,
	})
	endPoints = append(endPoints, kumoriv1.KukuEndpoint{
		Kind:    "deployment",
		Domain:  domain,
		Name:    solutionLink.TargetDeployment,
		Channel: solutionLink.TargetChannel,
	})
	kukuLinkSpec := kumoriv1.KukuLinkSpec{
		Endpoints: endPoints,
	}
	if solutionLink.Meta != nil {
		meta := runtime.RawExtension{Raw: solutionLink.Meta.Raw}
		kukuLinkSpec.Meta = &meta
	}

	kukuLinkID, err := naming.CalculateKukuLinkId(kukuLinkSpec)
	if err != nil {
		return
	}
	log.Infof("%s %s", meth, kukuLinkID)

	// From the spec:
	// - calculate a hash (sha1) of its content,
	// - get its manifest: the base64-encoded of the compressed (gzip) of the JSON
	//   version of the solution-link, but adding first several fields (name, owner)
	sha, manifest, err := t.getShaAndEncodedManifest(solutionLink, kukuSolution, kukuLinkID)
	if err != nil {
		return
	}

	// Annotations
	annotations := make(map[string]string)
	annotations[common.KukuLinkShaAnnotation] = sha
	annotations[common.KukuLinkManifestAnnotation] = manifest
	annotations[common.KukuLinkOwnerAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionOwnerAnnotation]
	annotations[common.KukuLinkSolutionNameAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionNameAnnotation]
	annotations[common.KukuLinkSolutionDomainAnnotation] = kukuSolution.GetAnnotations()[common.KukuSolutionDomainAnnotation]
	annotations[common.KukuLinkChannel1Annotation] = solutionLink.SourceChannel
	annotations[common.KukuLinkChannel2Annotation] = solutionLink.TargetChannel

	// Get v3dep ID from its name and solution
	sourceDeploymentID, found := v3depsDictionary[solutionLink.SourceDeployment]
	if !found {
		err = fmt.Errorf("when creating a KukuLink, the ID of the source v3deployment %s is not found", solutionLink.SourceDeployment)
		return
	}
	targetDeploymentID, found := v3depsDictionary[solutionLink.TargetDeployment]
	if !found {
		err = fmt.Errorf("when creating a KukuLink, the ID of the target v3deployment %s is not found", solutionLink.TargetDeployment)
		return
	}
	log.Infof("%s %s source and target deployment: %s, %s", meth, kukuLinkID, sourceDeploymentID, targetDeploymentID)

	// Labels
	labels := make(map[string]string)
	labels[common.KukuLinkOwnerLabel] = kukuSolution.GetLabels()[common.KukuSolutionOwnerLabel]
	labels[common.KukuLinkSolutionIdLabel] = kukuSolution.GetName()
	labels[common.KukuLinkSolutionNameLabel] = kukuSolution.GetLabels()[common.KukuSolutionNameLabel]
	labels[common.KukuLinkSolutionDomainLabel] = kukuSolution.GetLabels()[common.KukuSolutionDomainLabel]
	labels[common.KukuLinkChannel1Label] = naming.Hash(solutionLink.SourceChannel)
	labels[common.KukuLinkChannel2Label] = naming.Hash(solutionLink.TargetChannel)
	labels[common.KukuLinkDeployment1Label] = sourceDeploymentID
	labels[common.KukuLinkDeployment2Label] = targetDeploymentID
	labels[sourceDeploymentID] = common.KukuLinkDeployment1Label
	labels[targetDeploymentID] = common.KukuLinkDeployment2Label

	// Metadata
	meta := metav1.ObjectMeta{
		Name:      kukuLinkID,
		Namespace: kukuSolution.GetNamespace(),
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(kukuSolution, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindKukuSolution,
			}),
		},
	}
	meta.SetLabels(labels)
	meta.SetAnnotations(annotations)

	kukuLink = &kumoriv1.KukuLink{
		ObjectMeta: meta,
		Spec:       kukuLinkSpec,
	}

	return
}

// getShaAndEncodedManifest calculates, from the spec:
//   - calculate a hash (sha1) of its content,
//   - get its manifest: the base64-encoded of the compressed (gzip) of the JSON
//     version of the solution-link, but adding first several fields (name, owner)
func (t *KukuLinkTools) getShaAndEncodedManifest(
	solutionLink kumoriv1.Link,
	kukuSolution *kumoriv1.KukuSolution,
	kukuLinkID string,
) (
	hash string, encodedManifest string, err error,
) {
	owner := kukuSolution.GetLabels()[common.KukuSolutionOwnerLabel]
	linkPlus := LinkPlus{
		Link:  solutionLink,
		Owner: helper.UnscapeOwnerField(owner),
		Name:  kukuLinkID,
	}

	jsonLinkPlus, err := json.Marshal(linkPlus)
	if err != nil {
		return
	}
	hash = helper.CalculateSha(jsonLinkPlus)
	gzipLinkPlus, err := helper.GzipCompress(jsonLinkPlus)
	if err != nil {
		return
	}
	encodedManifest = helper.Base64Encode(gzipLinkPlus)
	return
}

// Search ...
func (t *KukuLinkTools) Search(kukuLink *kumoriv1.KukuLink, list []*kumoriv1.KukuLink) bool {
	for _, item := range list {
		if t.Equal(kukuLink, item) {
			return true
		}
	}
	return false
}

// Equal returns true if two KukuLink are equivalent.
func (t KukuLinkTools) Equal(
	o1, o2 *kumoriv1.KukuLink,
) bool {
	if o1 == nil && o2 == nil {
		return true
	}
	if o1 == nil && o2 != nil {
		return false
	}
	if o1 != nil && o2 == nil {
		return false
	}
	return apiequality.Semantic.DeepEqual(o1.Spec, o2.Spec)
}
