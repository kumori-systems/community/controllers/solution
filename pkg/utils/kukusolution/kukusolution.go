/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package kukusolution

import (
	"context"
	"fmt"

	common "solution-controller/pkg/utils/common"

	log "github.com/sirupsen/logrus"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	clientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// UpdateStatus is used to update an existing kukusolution in a Kubernetes
// cluster using a client-go instance.
func UpdateStatus(
	client clientset.Interface,
	kukuSolution *kumoriv1.KukuSolution,
) (*kumoriv1.KukuSolution, error) {
	if kukuSolution == nil {
		return nil, fmt.Errorf("KukuSolution is nil")
	}
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: common.FieldManager,
	}
	ns := kukuSolution.GetNamespace()
	name := kukuSolution.GetName()
	log.Debugf("KukuSolution.UpdateStatus. ConfigMap: %s", name)
	return client.KumoriV1().KukuSolutions(ns).UpdateStatus(ctx, kukuSolution, options)
}
