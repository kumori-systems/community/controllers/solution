/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package common

// Allowed actions for ExecuteActionInMap
const (
	ActionCreate = "create"
	ActionUpdate = "update"
	ActionDelete = "delete"
)

// Kind field in kubernetes manifests
const KindKukuSolution = "KukuSolution"

const FieldManager = "kumori-kukusolution-controller"

// Labels and annotations
const (
	KukuSolutionCommentAnnotation = "kumori/comment"
	KukuSolutionOwnerLabel        = "kumori/owner"
	KukuSolutionDomainLabel       = "kumori/domain"
	KukuSolutionNameLabel         = "kumori/name"
	KukuSolutionOwnerAnnotation   = "kumori/owner"
	KukuSolutionDomainAnnotation  = "kumori/domain"
	KukuSolutionNameAnnotation    = "kumori/name"

	V3depCommentAnnotation          = "kumori/comment"
	V3depLastModificationAnnotation = "kumori/lastModification"
	V3depShaAnnotation              = "kumori/sha"
	V3depManifestAnnotation         = "kumori/manifest"
	V3depOwnerAnnotation            = "kumori/owner"
	V3depSolutionDomainAnnotation   = "kumori/solution.domain"
	V3depSolutionNameAnnotation     = "kumori/solution.name"
	V3depNameAnnotation             = "kumori/name"
	V3depDomainAnnotation           = "kumori/domain"
	V3depOwnerLabel                 = "kumori/owner"
	V3depSolutionIdLabel            = "kumori/solution.id"
	V3depSolutionDomainLabel        = "kumori/solution.domain"
	V3depSolutionNameLabel          = "kumori/solution.name"
	V3depNameLabel                  = "kumori/name"
	V3depDomainLabel                = "kumori/domain"

	KukuLinkShaAnnotation            = "kumori/sha"
	KukuLinkManifestAnnotation       = "kumori/manifest"
	KukuLinkOwnerAnnotation          = "kumori/owner"
	KukuLinkSolutionDomainAnnotation = "kumori/solution.domain"
	KukuLinkSolutionNameAnnotation   = "kumori/solution.name"
	KukuLinkChannel1Annotation       = "channel1"
	KukuLinkChannel2Annotation       = "channel2"
	KukuLinkOwnerLabel               = "kumori/owner"
	KukuLinkSolutionIdLabel          = "kumori/solution.id"
	KukuLinkSolutionDomainLabel      = "kumori/solution.domain"
	KukuLinkSolutionNameLabel        = "kumori/solution.name"
	KukuLinkChannel1Label            = "channel1"
	KukuLinkChannel2Label            = "channel2"
	KukuLinkDeployment1Label         = "deployment1"
	KukuLinkDeployment2Label         = "deployment2"

	KukuResourceOwnerLabel  = "kumori/owner"
	KukuResourceDomainLabel = "kumori/domain"
	KukuResourceNameLabel   = "kumori/name"
	ResourceInUseLabel      = "resource-in-use"
)

// Message constants used with events
const (
	ReasonResourceCreated  = "Created"
	ReasonResourceUpdated  = "Updated"
	ReasonResourceDeleted  = "Deleted"
	MessageResourceCreated = "%s %s created"
	MessageResourceUpdated = "%s %s updated"
	MessageResourceDeleted = "%s %s deleted"
)
