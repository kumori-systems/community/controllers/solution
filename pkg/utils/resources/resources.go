/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package resources

import (
	"encoding/json"
	"fmt"
	common "solution-controller/pkg/utils/common"
	naming "solution-controller/pkg/utils/naming"
	"strings"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kukulisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	labels "k8s.io/apimachinery/pkg/labels"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// ResourceTools is just for manage resources in use
type ResourceTools struct {
	secretLister kukulisters.KukuSecretLister
	volumeLister kukulisters.KukuVolumeLister
	domainLister kukulisters.KukuDomainLister
	certLister   kukulisters.KukuCertLister
	caLister     kukulisters.KukuCaLister
	portLister   kukulisters.KukuPortLister
}

// NewResourceTools returns a new ResourceTools
func NewResourceTools(
	secretLister kukulisters.KukuSecretLister,
	volumeLister kukulisters.KukuVolumeLister,
	domainLister kukulisters.KukuDomainLister,
	certLister kukulisters.KukuCertLister,
	caLister kukulisters.KukuCaLister,
	portLister kukulisters.KukuPortLister,

) (t ResourceTools) {
	t = ResourceTools{
		secretLister: secretLister,
		volumeLister: volumeLister,
		domainLister: domainLister,
		certLister:   certLister,
		caLister:     caLister,
		portLister:   portLister,
	}
	return
}

// GetResourceID return the ID of a resource (kukusecret, kukuvolume, kukucas,
// kukucerts, kukuvhosts, kukutcpports
func (t *ResourceTools) GetResourceID(
	namespace string, solutionOwner string, solutionDomain string, resource kumoriv1.ConfigurationResource,
) (
	string, error,
) {
	if resource.Secret != nil {
		return t.getSecretID(namespace, solutionOwner, solutionDomain, *resource.Secret)
	} else if resource.Volume != nil {
		return t.getVolumeID(namespace, solutionOwner, solutionDomain, resource.Volume)
	} else if resource.Domain != nil {
		return t.getDomainID(namespace, solutionOwner, solutionDomain, *resource.Domain)
	} else if resource.Certificate != nil {
		return t.getCertificateID(namespace, solutionOwner, solutionDomain, *resource.Certificate)
	} else if resource.CA != nil {
		return t.getCaID(namespace, solutionOwner, solutionDomain, *resource.CA)
	} else if resource.Port != nil {
		return t.getPortID(namespace, solutionOwner, solutionDomain, *resource.Port)
	} else {
		return "", nil
	}
}

//
// TODO: all this functions are very-very-very similar! (to refactorize)
//

// getSecretID return the ID of a kukusecret
func (t *ResourceTools) getSecretID(
	namespace string, solutionOwner string, solutionDomain string, resource string,
) (
	string, error,
) {
	labelSelector, err := t.createLabelSelector(solutionOwner, solutionDomain, resource)
	if err != nil {
		return "", err
	}
	list, err := t.secretLister.KukuSecrets(namespace).List(labelSelector)
	if err != nil && !errors.IsNotFound(err) {
		return "", err
	}
	if len(list) > 1 {
		err = fmt.Errorf("there are several kukusecrets for %s-%s-%s", namespace, solutionOwner, resource)
		return "", err
	}
	if len(list) == 1 {
		return list[0].GetName(), nil
	}
	return "", nil
}

// getVolumeID return the ID of a kukuvolume
func (t *ResourceTools) getVolumeID(
	namespace string, solutionOwner string, solutionDomain string, resource *runtime.RawExtension,
) (
	string, error,
) {

	// Create a string containing the raw resource as a string
	var resourceStr string
	json.Unmarshal(resource.Raw, &resourceStr)

	// Check if it is a volatile volume (so resource is a JSON object) or a
	// persistent 	volume (so resource is a string)
	if strings.HasPrefix(resourceStr, "{") && strings.HasSuffix(resourceStr, "}") {
		// It is a volatile volume, so no resouce ID must be returned
		return "", nil
	}

	// The resource is a persistent volume, so its ID must be retrieved

	labelSelector, err := t.createLabelSelector(solutionOwner, solutionDomain, resourceStr)
	if err != nil {
		return "", err
	}
	list, err := t.volumeLister.KukuVolumes(namespace).List(labelSelector)
	if err != nil && !errors.IsNotFound(err) {
		return "", err
	}
	if len(list) > 1 {
		err = fmt.Errorf("there are several kukuvolumes for %s-%s-%s", namespace, solutionOwner, resource)
		return "", err
	}
	if len(list) == 1 {
		return list[0].GetName(), nil
	}
	return "", nil
}

// getDomainID return the ID of a kukudomain
func (t *ResourceTools) getDomainID(
	namespace string, solutionOwner string, solutionDomain string, resource string,
) (
	string, error,
) {
	labelSelector, err := t.createLabelSelector(solutionOwner, solutionDomain, resource)
	if err != nil {
		return "", err
	}
	list, err := t.domainLister.KukuDomains(namespace).List(labelSelector)
	if err != nil && !errors.IsNotFound(err) {
		return "", err
	}
	if len(list) > 1 {
		err = fmt.Errorf("there are several kukudomain for %s-%s-%s", namespace, solutionOwner, resource)
		return "", err
	}
	if len(list) == 1 {
		return list[0].GetName(), nil
	}
	return "", nil
}

// getCertificateID return the ID of a kukucert
func (t *ResourceTools) getCertificateID(
	namespace string, solutionOwner string, solutionDomain string, resource string,
) (
	string, error,
) {
	labelSelector, err := t.createLabelSelector(solutionOwner, solutionDomain, resource)
	if err != nil {
		return "", err
	}
	list, err := t.certLister.KukuCerts(namespace).List(labelSelector)
	if err != nil && !errors.IsNotFound(err) {
		return "", err
	}
	if len(list) > 1 {
		err = fmt.Errorf("there are several kukucerts for %s-%s-%s", namespace, solutionOwner, resource)
		return "", err
	}
	if len(list) == 1 {
		return list[0].GetName(), nil
	}
	return "", nil
}

// getCaID return the ID of a kukuca
func (t *ResourceTools) getCaID(
	namespace string, solutionOwner string, solutionDomain string, resource string,
) (
	string, error,
) {
	labelSelector, err := t.createLabelSelector(solutionOwner, solutionDomain, resource)
	if err != nil {
		return "", err
	}
	list, err := t.caLister.KukuCas(namespace).List(labelSelector)
	if err != nil && !errors.IsNotFound(err) {
		return "", err
	}
	if len(list) > 1 {
		err = fmt.Errorf("there are several kukucas for %s-%s-%s", namespace, solutionOwner, resource)
		return "", err
	}
	if len(list) == 1 {
		return list[0].GetName(), nil
	}
	return "", nil
}

// getPortID return the ID of a kukuport
func (t *ResourceTools) getPortID(
	namespace string, solutionOwner string, solutionDomain string, resource string,
) (
	string, error,
) {
	labelSelector, err := t.createLabelSelector(solutionOwner, solutionDomain, resource)
	if err != nil {
		return "", err
	}
	list, err := t.portLister.KukuPorts(namespace).List(labelSelector)
	if err != nil && !errors.IsNotFound(err) {
		return "", err
	}
	if len(list) > 1 {
		err = fmt.Errorf("there are several kukuport for %s-%s-%s", namespace, solutionOwner, resource)
		return "", err
	}
	if len(list) == 1 {
		return list[0].GetName(), nil
	}
	return "", nil
}

// createLabelSelector create a label selector for a resource
// Take into account that, currently:
// - The "resource" string could contain just the name (without domain) of the resource
// - In that case, it is assumed that the domain string is the same as the
//   kukusolution (that, in fact, is equal to the owner)
func (t *ResourceTools) createLabelSelector(solutionOwner, solutionDomain, resource string) (labels.Selector, error) {

	domain := ""
	name := ""
	parts := strings.Split(resource, "/")
	if len(parts) == 2 {
		domain = parts[0]
		name = parts[1]
	} else if len(parts) == 1 {
		domain = solutionDomain
		name = parts[0]
	} else {
		return nil, fmt.Errorf("invalid domain/name: %s", resource)
	}

	matchLabels := map[string]string{
		common.KukuResourceDomainLabel: naming.Hash(domain),
		common.KukuResourceNameLabel:   naming.Hash(name),
	}
	return labels.SelectorFromSet(matchLabels), nil
}
