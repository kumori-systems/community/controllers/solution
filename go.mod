module solution-controller

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/sirupsen/logrus v1.8.2
	gitlab.com/kumori-systems/community/libraries/base-controller v1.1.0
	gitlab.com/kumori-systems/community/libraries/client-go v1.8.0
	k8s.io/api v0.25.6
	k8s.io/apimachinery v0.25.6
	k8s.io/client-go v0.25.6
)

// replace gitlab.com/kumori-systems/community/libraries/base-controller v1.1.0 => gitlab.com/kumori/platform/controllers/libraries/base-controller.git v1.1.0

// replace gitlab.com/kumori-systems/community/libraries/base-controller v1.1.0 => /home/jbgisber/projects/git/libraries/base-controller

// replace gitlab.com/kumori-systems/community/libraries/client-go v1.2.0 => gitlab.com/kumori/platform/controllers/libraries/client-go.git v1.5.0

// replace gitlab.com/kumori-systems/community/libraries/client-go v1.7.0 => /home/jbgisber/projects/git/libraries/client-go
