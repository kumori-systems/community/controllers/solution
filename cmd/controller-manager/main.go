/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package main

import (
	"flag"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	solutioncontroller "solution-controller/pkg/controllers/kukusolution"
	"time"

	stopsignal "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/stop-signal"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kukuinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"

	log "github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes"
	rest "k8s.io/client-go/rest"
	clientcmd "k8s.io/client-go/tools/clientcmd"
)

// Ugly! We would prefer an automatic version numbering
var controllerVersion string

// How many workers will be launched
const threadiness int = 1

// Command-line args (see init function)
var version bool
var kubeconfig string
var resyncInterval time.Duration
var revisionHistoryLimit int

// init reads command-line args (init function is executed when package is imported)
func init() {
	controllerVersion = "Solution Controller v3.0.0"
	flag.BoolVar(&version, "version", false, "Prints current controller version")
	flag.StringVar(&kubeconfig, "kubeconfig", "",
		"Paths to a kubeconfig. Only required if out-of-cluster.")
	flag.DurationVar(&resyncInterval, "resync", 0,
		"Resync interval (5s, 1m...). Never, if not specified or 0.")
	flag.IntVar(&revisionHistoryLimit, "revisionHistoryLimit", 10,
		"The maximum number of versions stored per V3Deployment (excluding the live one)")
}

// getConfig loads a REST Config, depending of the environment and flags:
// Config precedence:
// * --kubeconfig flag pointing at a file
// * KUBECONFIG environment variable pointing at a file
// * In-cluster config if running in cluster
// * $HOME/.kube/config if exists (default location in the user's home directory)
func getConfig() (config *rest.Config, err error) {
	if len(kubeconfig) > 0 {
		return clientcmd.BuildConfigFromFlags("", kubeconfig)
	}
	if len(os.Getenv("KUBECONFIG")) > 0 {
		return clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	}
	if c, err := rest.InClusterConfig(); err == nil {
		return c, nil
	}
	if usr, err := user.Current(); err == nil {
		c, err := clientcmd.BuildConfigFromFlags("", filepath.Join(usr.HomeDir, ".kube", "config"))
		if err == nil {
			return c, nil
		}
	}
	return nil, fmt.Errorf("could not locate a kubeconfig")
}

// setQPSBurst applies saner defaults for QPS and burst based on the Kubernetes
// controller manager defaults (20 QPS, 30 burst)
// These 2 flags set normal and burst rate that controller can talk to
// kube-apiserver.
// Default values work pretty well (20 for QPS and 30 for burst).
// Increase these values for large, production clusters.
func setQPSBurst(config *rest.Config) {
	if config.QPS == 0.0 {
		config.QPS = 20.0
		config.Burst = 30.0
	}
}

// main is the entrypoint of the controller
func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetLevel(log.DebugLevel)

	// Set up signals so we handle the shutdown signal gracefully
	stopCh := stopsignal.SetupStopSignal()

	flag.Parse()
	if version {
		fmt.Println(controllerVersion)
		return
	}

	log.Info("main.main() Running solutioncontroller...")
	config, err := getConfig()
	if err != nil {
		log.Fatal("main.main() Error getting Kubernetes config: ", err)
	}
	setQPSBurst(config)

	// Create a clientset and informer-factory for kubernetes standard objects
	kubeClientset, err := kubeclientset.NewForConfig(config)
	if err != nil {
		log.Fatal("main.main() Error building kubernetes clientset: ", err)
	}
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset,
		resyncInterval,
		"",
		func(opt *metav1.ListOptions) {},
	)

	// Create a clientset and informer-factory for Kuku objects
	kukuClientset, err := kukuclientset.NewForConfig(config)
	if err != nil {
		log.Fatal("main.main() Error creating the kuku clientset: ", err)
	}
	kukuInformerFactory := kukuinformers.NewFilteredSharedInformerFactory(
		kukuClientset,
		resyncInterval,
		"",
		func(opt *metav1.ListOptions) {},
	)

	// Create the controllers (only one, in this case)
	solutionController := solutioncontroller.NewController(
		kubeClientset,
		kukuClientset,
		kukuInformerFactory.Kumori().V1().KukuSolutions(),     // Primary
		kukuInformerFactory.Kumori().V1().V3Deployments(),     // Secondary
		kukuInformerFactory.Kumori().V1().KukuLinks(),         // Secondary
		kubeInformerFactory.Apps().V1().ControllerRevisions(), // Special secondary
		kukuInformerFactory.Kumori().V1().KukuSecrets(),       // Terciary
		kukuInformerFactory.Kumori().V1().KukuVolumes(),       // Terciary
		kukuInformerFactory.Kumori().V1().KukuDomains(),       // Terciary
		kukuInformerFactory.Kumori().V1().KukuCerts(),         // Terciary
		kukuInformerFactory.Kumori().V1().KukuCas(),           // Terciary
		kukuInformerFactory.Kumori().V1().KukuPorts(),         // Terciary
		revisionHistoryLimit,
	)

	// Start informers
	kubeInformerFactory.Start(stopCh)
	kukuInformerFactory.Start(stopCh)

	// Start controllers
	errCh := make(chan error)
	go solutionController.Run(threadiness, stopCh, errCh)

	select {
	case err := <-errCh: // Detect errors starting controllers
		fmt.Println("main.main() Error running controller: ", err)
	case <-stopCh: // Wait application closed
		log.Info("main.main() Closing solutioncontroller")
	}
}
